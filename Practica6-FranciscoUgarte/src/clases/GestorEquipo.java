package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorEquipo {
	private ArrayList<Equipo> listaEquipos;
	private ArrayList<Jugador> listaJugadores;
	
	public GestorEquipo() {
		listaEquipos = new ArrayList<Equipo>();
		listaJugadores = new ArrayList<Jugador>();
	}
	
	public void altaJugador(String nombre, String nacionalidad, double valor) {
		if ((!existeJugador(nombre))) {
			Jugador nuevoJugador = new Jugador(nombre, nacionalidad, valor);
			nuevoJugador.setFechaFinContrato(LocalDate.now());
			listaJugadores.add(nuevoJugador);
		} else {
			System.out.println("El jefe ya existe");
		}
	}

	public boolean existeJugador(String nombre) {
		for (Jugador jugador : listaJugadores) {
			if (jugador != null && jugador.getNombreJugador().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	public void listaJugadores() {
		for (Jugador jugador : listaJugadores) {
			if (jugador != null) {
				System.out.println(jugador);
			}
		}
	}

	public Jugador buscarJugador(String nombre) {
		for (Jugador capitan : listaJugadores) {
			if (capitan != null && capitan.getNombreJugador().equals(nombre)) {
				return capitan;
			}
		}
		return null;
	}

	public void altaEquipo(String nombreEquipo, String nombreEstadio, int titulos, String anoFundacion) {
		Equipo nuevoEquipo = new Equipo(nombreEquipo, nombreEstadio, titulos);
		nuevoEquipo.setAnoFundacion(LocalDate.parse(anoFundacion)); 
		listaEquipos.add(nuevoEquipo);
	}

	public void listarEquipos() {
		for (Equipo equipo : listaEquipos) {
			if (equipo != null) {
				System.out.println(equipo);
			}
		}
	}

	public void eliminarEquipo(String nombreEquipo) {
		Iterator<Equipo> iteradorEquipos = listaEquipos.iterator();

		while (iteradorEquipos.hasNext()) {
			Equipo equipo = iteradorEquipos.next();
			if (equipo.getNombreEquipo().equals(nombreEquipo)) {
				iteradorEquipos.remove();
			}
		}
	}

	public void asignarJugador(String nombre, String nombreEquipo) {
		if ( buscarJugador(nombre) != null && buscarEquipo(nombreEquipo)!= null) {
			Jugador capitan = buscarJugador(nombre);
			Equipo equipo = buscarEquipo(nombreEquipo);
			equipo.setJugadores(capitan);
		}
	}


                                     
	public Jugador buscarJugadorExperto() {
		LocalDate fechaAntigua = null;
		for (int i = 0; i < listaJugadores.size(); i++) {
			Jugador jugadorActual = listaJugadores.get(i);
			if (jugadorActual != null && i == 0) {
				fechaAntigua = jugadorActual.getFechaFinContrato();
			} else {
				if (jugadorActual != null && jugadorActual.getFechaFinContrato().isAfter(fechaAntigua)) {
					fechaAntigua = jugadorActual.getFechaFinContrato();
				}
			}
		}
		for (Jugador capitan : listaJugadores) {
			if (capitan != null && capitan.getFechaFinContrato().equals(fechaAntigua)) {
				return capitan;
			}
		}
		return null;
	}

	public Equipo buscarEquipo(String nombreEquipo) {
		for (Equipo equipo : listaEquipos) {
			if (equipo != null && equipo.getNombreEquipo().equals(nombreEquipo)) {
				return equipo;
			}
		}
		return null;
	}

	public void listarEquiposAnno(int anno) {
		for (Equipo equipo : listaEquipos) {
			if (equipo.getAnoFundacion().getYear() == anno) {
				System.out.println(equipo);
			}
		}
	}

	public void listarEquiposDeJugadores(String nombre) {
		for (Equipo equipo : listaEquipos) {
			if (equipo.getJugadores() != null && equipo.getJugadores().getNombreJugador().equals(nombre)) {
				System.out.println(equipo+nombre);
			}
		}
	}

	public void fichajes() {
		Iterator<Jugador> iteradorJugadores = listaJugadores.iterator();

		while (iteradorJugadores.hasNext()) {
			Jugador jugadorActual = iteradorJugadores.next();
			boolean estaEnEquipo = false;
			for (Equipo equipo : listaEquipos) {
				if (equipo.getJugadores() != null
						&& equipo.getJugadores().getNombreJugador().equals(jugadorActual.getNombreJugador())) {
					estaEnEquipo = true;
				}
			}
			if (!estaEnEquipo) {
				iteradorJugadores.remove();
			}
		}
	}
	
	public void jugadorMasValioso() {
		
	}
}
