package clases;

import java.time.LocalDate;

public class Equipo {
	private String nombreEquipo;
	private String nombreEstadio;
	private LocalDate anoFundacion;
	private int titulos;
	private Jugador jugadores;

	
	public Equipo(String nombreEquipo, String nombreEstadio, int titulos) {
		this.nombreEquipo = nombreEquipo;
		this.nombreEstadio = nombreEstadio;
		this.titulos = titulos;
	}
	
	public String getNombreEquipo() {
		return nombreEquipo;
	}
	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}
	public String getNombreEstadio() {
		return nombreEstadio;
	}
	public void setNombreEstadio(String nombreEstadio) {
		this.nombreEstadio = nombreEstadio;
	}
	public LocalDate getAnoFundacion() {
		return anoFundacion;
	}
	public void setAnoFundacion(LocalDate anoFundacion) {
		this.anoFundacion = anoFundacion;
	}
	public int getTitulos() {
		return titulos;
	}
	public void setTitulos(int titulos) {
		this.titulos = titulos;
	}
	public Jugador getJugadores() {
		return jugadores;
	}
	public void setJugadores(Jugador jugadores) {
		this.jugadores = jugadores;
	}
	@Override
	public String toString() {
		return "Equipo [nombreEquipo=" + nombreEquipo + ", nombreEstadio=" 
				+ nombreEstadio + ", anoFundacion="
				+ anoFundacion + ", titulos=" + titulos + ", jugadores=" + jugadores+  "]";
	}
	
	
}
