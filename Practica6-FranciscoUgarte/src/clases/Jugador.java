package clases;

import java.time.LocalDate;

public class Jugador {
	private String nombreJugador;
	private String nacionalidad;
	private LocalDate fechaFinContrato;
	private double valor;
	
	public Jugador(String nombreJugador, String nacionalidad, double valor) {
		this.nombreJugador = nombreJugador;
		this.nacionalidad = nacionalidad;
		this.valor = valor;
	}
	
	public String getNombreJugador() {
		return nombreJugador;
	}
	public void setNombreJugador(String nombreJugador) {
		this.nombreJugador = nombreJugador;
	}
	public LocalDate getFechaFinContrato() {
		return fechaFinContrato;
	}
	public void setFechaFinContrato(LocalDate fechaFinContrato) {
		this.fechaFinContrato = fechaFinContrato;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getNacionalidado() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	@Override
	public String toString() {
		return "Jugador [nombreJugador=" + nombreJugador
				+ ", nacionalidad=" + nacionalidad + ", fechaFinContrato="
				+ fechaFinContrato + ", valor=" + valor + "]";
	}
	
	
	
}
