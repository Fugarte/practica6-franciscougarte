package programa;

import java.util.Scanner;

import clases.GestorEquipo;

public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("1");
		GestorEquipo equipo = new GestorEquipo();
		
		System.out.println("2");
		System.out.println("alta jugador");
		System.out.println("Ingrese el nombre del primer jugador:");
		String nombre1 = input.nextLine();
		System.out.println("Su nacionalidad:");
		String nacionalidad1 = input.nextLine();
		System.out.println("Su valor (con decimales):");
		double valor1 = input.nextDouble();
		
		input.nextLine();
		
		System.out.println("Ingrese el nombre del segundo jugador:");
		String nombre2 = input.nextLine();
		System.out.println("Su nacionalidad:");
		String nacionalidad2 = input.nextLine();
		System.out.println("Su valor (con decimales):");
		double valor2 = input.nextDouble();
		
		input.nextLine();
		
		System.out.println("Ingrese el nombre del tercer jugador:");
		String nombre3 = input.nextLine();
		System.out.println("Su nacionalidad:");
		String nacionalidad3 = input.nextLine();
		System.out.println("Su valor (con decimales):");
		double valor3 = input.nextDouble();
		
		input.nextLine();
		
		equipo.altaJugador(nombre1, nacionalidad1, valor1);
		equipo.altaJugador(nombre2, nacionalidad2, valor2);
		equipo.altaJugador(nombre3, nacionalidad3, valor3);
		
		System.out.println("3");
		System.out.println("Listamos jugadores");
		equipo.listaJugadores();
		
		System.out.println("4");
		System.out.println("Buscamos jugador");
		System.out.println("Buscamos el primer jugador introducido");
		System.out.println(equipo.buscarJugador(nombre1));
		System.out.println("Buscamos el segundo jugador introducido");
		System.out.println(equipo.buscarJugador(nombre2));
		
		System.out.println("Ingrese el nombre del primer equipo:");
		String equipo1 = input.nextLine();
		System.out.println("Ingrese el nombre del primer equipo:");
		String equipo2 = input.nextLine();
		System.out.println("Ingrese el nombre del primer equipo:");
		String equipo3 = input.nextLine();
		
		System.out.println("5");
		System.out.println("alta equipo");
		equipo.altaEquipo(equipo1, "Romareda", 11, "1932-03-18");
		equipo.altaEquipo(equipo2, "Castalia", 3, "1919-03-12");
		equipo.altaEquipo(equipo3, "Mestalla", 29, "1919-03-18");
		
		System.out.println("6");
		System.out.println("Asignar jugador a equipo por orden de ingreso");
		equipo.asignarJugador(nombre1, equipo1);
		equipo.asignarJugador(nombre2, equipo2);
		equipo.asignarJugador(nombre3, equipo3);
		
		System.out.println("7");
		System.out.println("Listar equipos por jugador");
		System.out.println("Listar equipos primer jugador");
		equipo.listarEquiposDeJugadores(nombre1);
		System.out.println("Listar equipos segundo jugador");
		equipo.listarEquiposDeJugadores(nombre2);
		System.out.println("Listar equipos tercer jugador");
		equipo.listarEquiposDeJugadores(nombre3);
		
		System.out.println("8");
		System.out.println("listar equipos");
		equipo.listarEquipos();
		System.out.println("listar equipos por a�o");
		equipo.listarEquiposAnno(1919);
		
		System.out.println("9");
		System.out.println("Eliminar el segundo equipo introducido");
		equipo.eliminarEquipo(equipo2);
		System.out.println("listar equipos");
		equipo.listarEquipos();
		
		System.out.println("10");
		System.out.println("Listar equipos por jugador");
		System.out.println("Listar equipos jugador javi");
		equipo.listarEquiposDeJugadores("javi");
		System.out.println("Listar equipos jugador jose");
		equipo.listarEquiposDeJugadores("jose");
		
		System.out.println("11");
		System.out.println("fichajes");
		System.out.println("Listamos jugadores antes de fichajes");
		equipo.listaJugadores();
		equipo.fichajes();
		System.out.println("Listamos jugadores despues de fichajes");
		equipo.listaJugadores();
		
		System.out.println("12");
		System.out.println("Buscamos el jugador mas experto");
		System.out.println(equipo.buscarJugadorExperto());
		
		input.close();
	}

}
